Projekt idea je "Task treker". Umoznuje userovi vytvaret tridy ukolu. V tehto tridach si vytvari nejake ukoly. Kazdy ukol ma nazev a popis. K tomu jeste lze pridat cas pro vypracovani, a usera, ktery je zoodpoveden za ten ukol. Pokud se nejaky ukol blizi se k deadlinu, tak zmeni barvu tridy na cerveny, pokud vsechny ukoly je oznacene "Ok", tak barva tridy bude zmenena na zelenou


Komplexni dotaz : Admin muze odstranit usera z "Group" nebo zakazat mu editovat tasky

Komplexni operace : Zavola operaci bool userDelete( id_user ). Pokud user neni v "Group", tak vraci false. Jinak odstrani mu pristup a vraci false. Pokud chci zakazat ci dat pristup k editaci : changeStatus (id_user, bool allow)
